const express = require("express");
const router = express.Router();
const resH = require("../helpers/response-helper");
const MovieModel = require("../models/movie");
const movieSchema = require("../models/schemas/movie");
const querySchema = require("../models/schemas/query");

const fs = require("fs");

router.get("/", async (req, res) => {
    try {
        await querySchema.validateAsync(req.query);
        const duration = Number(req.query.duration);
        const genres = req.query.genres;

        const movies = await MovieModel.getMoviesByParameters(duration, genres);
        resH.send(res, movies.data, true, 200, movies.message);
    } catch (error) {
        console.log(error);
        resH.send(res, null, false, 500, error.message);
    }
});

router.post("/", async (req, res) => {
    try {
        console.log(req.body);
        await movieSchema.validateAsync(req.body);
        const updatedMovies = await MovieModel.addNewMovie(req.body);
        resH.send(res, updatedMovies, true, 200, "new movie added!");
    } catch (error) {
        console.log(error);
        resH.send(res, null, false, 500, error.message);
    }
});

module.exports = router;
