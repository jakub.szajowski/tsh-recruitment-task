const express = require('express');

/**
 * Creates and sends the response of the HTTP request to the client encapsulated in envelope. Takes the 'res' argument and uses it to actually send the request.
 * @param {*} res - The 'res' object of Express
 * @param {Object} data - Data to be sent
 * @param {Boolean} success - If the request was successful or not. Defaults to 'true'.
 * @param {*} code - The HTTP code of the request. Default is 200.
 * @param {String} message - Optional message string of the response's envelope.
 */

const send = function(res, data, success = true, code = 200, message = null){
    let obj = {};
    if (success) {
        obj.status = "success";
    } else {
        obj.status = "error";
    }
    obj.data = data;
    obj.message = message;
    res.status(code).json(obj);
}

module.exports = {send: send};