const Joi = require("joi");
const fs = require("fs");
const db = JSON.parse(fs.readFileSync("./data/db.json", "utf8"));

const querySchema = Joi.object({
    duration: Joi.number(),

    genres: Joi.array().items(Joi.string().valid(...db.genres)),
});

module.exports = querySchema;
