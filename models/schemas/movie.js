const Joi = require('joi');
const fs = require('fs');
const db = JSON.parse(fs.readFileSync('./data/db.json', 'utf8'));

const movieSchema = Joi.object({
    id: Joi.number()
        .forbidden,

    title: Joi.string()
        .max(225)
        .required(),

    year: Joi.number()
        .required(),

    runtime: Joi.number()
        .required(),

    genres: Joi.array()
        .required()
        .items(Joi.string().valid(...db.genres)),

    director: Joi.string()
        .max(225)
        .required(),

    actors: Joi.string()
        .optional(),

    plot: Joi.string()
        .optional(),

    posterUrl: Joi.string()
        .optional()
});

module.exports = movieSchema;