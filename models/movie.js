const fs = require("fs");
const _ = require("lodash");

const MovieModel = {};

MovieModel.addNewMovie = async (movie) => {
    let db = JSON.parse(fs.readFileSync("./data/db.json", "utf8"));
    let newMovie = {
        id: db.movies[db.movies.length - 1].id + 1,
        title: movie.title,
        year: movie.year,
        runtime: movie.runtime,
        genres: movie.genres,
        director: movie.director,
        actors: movie.actors,
        plot: movie.plot,
        posterUrl: movie.posterUrl,
    };

    db.movies.push(newMovie);
    fs.writeFileSync("./data/db.json", JSON.stringify(db), "utf8");

    return newMovie;
};

MovieModel.getMoviesByParameters = (duration, genres) => {
    const db = JSON.parse(fs.readFileSync("./data/db.json", "utf8"));
    let movies = db.movies;

    if (duration)
        movies = movies.filter(
            (movie) =>
                duration - 10 <= movie.runtime && duration + 10 >= movie.runtime
        );
    if (genres)
        movies = movies.filter((movie) =>
            genres.some((genre) => movie.genres.includes(genre))
        );

    if ((duration && genres) || genres)
        return {
            data: movies
                .map((movie) => ({
                    movie,
                    matching: movie.genres.filter((genre) =>
                        genres.includes(genre)
                    ).length,
                }))
                .sort((a, b) => b.matching - a.matching)
                .map((movie) => movie.movie),
            message: `${movies.length} movies found`,
        };
    else
        return {
            data: _.shuffle(movies).pop(),
            message: `${movies.length} movies found. Here is one of them.`,
        };
};

module.exports = MovieModel;
