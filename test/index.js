const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index.js");
const should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);
chai.should();
chai.use(require("chai-things"));

describe("API Home endpoint", () => {
    it("should return A TEAPOT HTTP STATUS on / GET", (done) => {
        chai.request(server)
            .get("/")
            .end((err, res) => {
                expect(res).to.have.status(418);
                done();
            });
    });
});

describe("API Movies POST endpoint", () => {
    it('should NOT add new movie without "title" parameter on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"title" is required');
                done();
            });
    });

    it('should NOT add new movie without "year" parameter on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                director: "Mr Tester",
                runtime: 420,
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"year" is required');
                done();
            });
    });

    it('should NOT add new movie without "director" parameter on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2022,
                runtime: 420,
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"director" is required');
                done();
            });
    });

    it('should NOT add new movie without "runtime" parameter on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2022,
                director: "Mr Tester",
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"runtime" is required');
                done();
            });
    });

    it('should NOT add new movie without "genres" parameter on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"genres" is required');
                done();
            });
    });

    it('should NOT add new movie with wrong "title" parameter type on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: { title: "The Test Movie" },
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"title" must be a string');
                done();
            });
    });

    it('should NOT add new movie with wrong "year" parameter type on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: "",
                director: "Mr Tester",
                runtime: 420,
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"year" must be a number');
                done();
            });
    });

    it('should NOT add new movie with wrong "director" parameter type on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2020,
                director: ["Mr Tester", "Mrs Tester"],
                runtime: 420,
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain(
                    '"director" must be a string'
                );
                done();
            });
    });

    it('should NOT add new movie with wrong "runtime" parameter type on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2020,
                director: "Mr Tester",
                runtime: [420],
                genres: ["Crime", "Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain(
                    '"runtime" must be a number'
                );
                done();
            });
    });

    it('should NOT add new movie with wrong "genres" parameter type on /movies POST', (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2020,
                director: "Mr Tester",
                runtime: 420,
                genres: "Drama",
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain(
                    '"genres" must be an array'
                );
                done();
            });
    });

    it("should NOT add new movie with wrong genre on /movies POST", (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
                genres: ["Drama", "bajo jajo"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain(
                    '"genres[1]" must be one of'
                );
                done();
            });
    });

    it("should NOT add new movie with wrong property on /movies POST", (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
                genres: ["Drama"],
                test: "I should not be here",
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain('"test" is not allowed');
                done();
            });
    });

    it("should NOT add new movie with too long title on /movies POST", (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII",
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
                genres: ["Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain(
                    '"title" length must be less than or equal to 225 characters long'
                );
                done();
            });
    });

    it("should NOT add new movie with too long director on /movies POST", (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "Test",
                year: 2022,
                director:
                    "IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII",
                runtime: 420,
                genres: ["Drama"],
            })
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain(
                    '"director" length must be less than or equal to 225 characters long'
                );
                done();
            });
    });

    it("should add new movie on /movies POST", (done) => {
        chai.request(server)
            .post("/movies")
            .send({
                title: "The Test Movie",
                year: 2022,
                director: "Mr Tester",
                runtime: 420,
                genres: ["Crime", "Drama"],
                actors: "Test Actor, Test Actress",
                plot: "This is a test",
                posterUrl: "https://",
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an("object");
                expect(res.body.data).to.be.an("object");
                expect(res.body.message).to.equal("new movie added!");

                expect(res.body.data).to.have.keys(
                    "id",
                    "title",
                    "year",
                    "director",
                    "runtime",
                    "genres",
                    "actors",
                    "plot",
                    "posterUrl"
                );

                expect(res.body.data.id).to.be.a("number");
                expect(res.body.data.title)
                    .to.be.a("string")
                    .and.equal("The Test Movie");
                expect(res.body.data.year).to.be.a("number").and.equal(2022);
                expect(res.body.data.director)
                    .to.be.a("string")
                    .and.equal("Mr Tester");
                expect(res.body.data.runtime).to.be.a("number").and.equal(420);
                expect(res.body.data.genres)
                    .to.be.an("array")
                    .and.eql(["Crime", "Drama"]);
                expect(res.body.data.actors)
                    .to.be.a("string")
                    .and.equal("Test Actor, Test Actress");
                expect(res.body.data.plot)
                    .to.be.a("string")
                    .and.equal("This is a test");
                expect(res.body.data.posterUrl)
                    .to.be.a("string")
                    .and.equal("https://");

                done();
            });
    });
});

describe("API Movies GET endpoint", () => {
    it("should get one random movie on /movies GET", (done) => {
        chai.request(server)
            .get("/movies")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an("object");

                expect(res.body.data).to.have.property("id");
                expect(res.body.data).to.have.property("title");
                expect(res.body.data).to.have.property("year");
                expect(res.body.data).to.have.property("director");
                expect(res.body.data).to.have.property("runtime");
                expect(res.body.data).to.have.property("genres");

                done();
            });
    });

    it("should get one random movie on /movies?duration=100 GET", (done) => {
        chai.request(server)
            .get("/movies?duration=100")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an("object");
                expect(Number(res.body.data.runtime)).to.be.within(90, 110);
                done();
            });
    });

    it("should get movies of specific genre on movies/?genres[]=Music&genres[]=Drama&genres[]=Crime GET", (done) => {
        chai.request(server)
            .get("/movies/?genres[]=Music&genres[]=Drama&genres[]=Crime")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an("array");
                expect(res.body.data[0].genres).to.have.members([
                    "Music",
                    "Crime",
                    "Drama",
                ]);

                done();
            });
    });

    it("should get movies of specific genre narrowed by duration on movies/?genres[]=Music&duration= GET", (done) => {
        chai.request(server)
            .get("/movies/?genres[]=Music&duration=120")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an("array");

                expect(res.body.data).all.have.property("genres");
                expect(res.body.data).all.have.property("runtime");

                expect(res.body.data[0].genres).to.contain("Music");
                expect(Number(res.body.data[0].runtime)).to.be.within(
                    120 - 10,
                    120 + 10
                );

                expect(
                    res.body.data[res.body.data.length - 1].genres
                ).to.contain("Music");
                expect(
                    Number(res.body.data[res.body.data.length - 1].runtime)
                ).to.be.within(120 - 10, 120 + 10);

                done();
            });
    });

    it("should NOT get any movie on /movies?duration=test GET", (done) => {
        chai.request(server)
            .get("/movies?duration=test")
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.equal(
                    '"duration" must be a number'
                );
                done();
            });
    });

    it("should NOT get any movie on /movies?genres=Music GET", (done) => {
        chai.request(server)
            .get("/movies?genres=Music")
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.equal('"genres" must be an array');
                done();
            });
    });

    it("should NOT get any movie on /movies?genres[]=Test GET", (done) => {
        chai.request(server)
            .get("/movies?genres[]=Test")
            .end((err, res) => {
                expect(res).to.have.status(500);
                expect(res.body.status).to.equal("error");
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.contain("must be one of");
                done();
            });
    });
});
