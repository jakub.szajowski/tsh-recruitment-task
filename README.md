# tsh-recruitment-task

Node.js backend

## Contents

- [Endpoints](#endpoints)
- [Schemas](#schemas)

## Endpoints

### Movies

#### `/movies` `GET`

Return a single random movie.

#### `/movies?duration=<number>` `GET`

Return a single random movie that has a runtime between <duration - 10> and <duration + 10>.

#### `/movies?genres[]=<genre>` `GET`

Return all movies that contain at least one of the specified genres. Orderd by a number of genres that match.

#### `/movies?duration=<number>&genres[]=<genre>` `GET`

Return an array of all movies that contain at least one of the specified genres narrowed by a runtime. Orderd by a number of genres that match.

- duration
- array of genres

⚠️ Caution! Only predefined genres allowed.

#### `/movies` `POST`

Adds new movie to collection.

sample request body:

```javascript
{
    "title": "The Test Movie",
    "year": 1,
    "director": "Mr Tester",
    "runtime": 420,
    "genres": ["Crime","Drama"]
}
```

## Schemas

### Movie

```javascript
const movieSchema = Joi.object({
  id: Joi.number().forbidden,

  title: Joi.string().max(225).required(),

  year: Joi.number().required(),

  runtime: Joi.number().required(),

  genres: Joi.array()
    .required()
    .items(Joi.string().valid(...db.genres)),

  director: Joi.string().max(225).required(),

  actors: Joi.string().optional(),

  plot: Joi.string().optional(),

  posterUrl: Joi.string().optional(),
});
```

### query

```javascript
const querySchema = Joi.object({
  duration: Joi.number(),

  genres: Joi.array().items(Joi.string().valid(...db.genres)),
});
```
