const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const bodyParser = require("body-parser");

const app = express();
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());

const http = require("http").Server(app);

//routers
const moviesRouter = require("./routes/movies");

//routers use
app.use("/movies", moviesRouter);

app.get("/", function (req, res) {
    res.status(418).send(
        "Hello, this is recruitment-task-backend API. How can I help you?"
    );
});

let server = http.listen(3000, function () {});

let stop = function () {
    server.close();
};

module.exports = server;
module.exports.stop = stop;
